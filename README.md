Evaluation of SAC, PPO and DDPG using (1) Laelaps II model in Gazebo, (2) ROS and (3) Gym. Work by Yehia S. Elbahrawy supervised by A. S. Mastrogergiou.

Laelaps II is a quadruped robot built by the [Legged Team at the Control Systems Lab of NTUA](http://nereus.mech.ntua.gr/legged/).

# Steps to train Laelaps II using DDPG, SAC and PPO:

### Spawn Laelaps II in Gazebo and choose one of the following training tasks:
	
Positive slope inclination with 10 degrees:

	roslaunch laelaps_gazebo laelaps_world_upRamp.launch

### Launch the training environment and the algorithm:
1) Deep Deterministic Policy Gradient (DDPG): 

		roslaunch training_system training_ddpg_RL_Ellipse_gaits.launch

2) Soft Actor-Critic (SAC): 
	
		roslaunch training_system training_sac_RL_Ellipse_gaits.launch

3) Proximal Policy Optimization (PPO): 
	
		roslaunch training_system training_ppo_RL_Ellipse_gaits.launch

# Steps to test the generalization of a pre-trained saved model: 

### Spawn Laelaps II in Gazebo and choose one of the following training tasks depending on the trained model:

1) Positive slope inclination with varying inclination: 
	
		roslaunch laelaps_gazebo laelaps_world_upRamp_10_8_15deg_3ramps.launch 

2) Positive slope inclination with 10 degrees with longer ramp: 
	
		roslaunch laelaps_gazebo laelaps_world_upRamp_4ramps.launch

### Launch pre-trained saved model:
		
1) Choose a saved model from the runs folder in the training_system package, where you can find your training results. Inside the training_results folder, the saved models are in the Model_best_test_rewards folder.
	
2) Test the algorithm pre-saved model:

#### DDPG
	roslaunch training_system test_pretrained_model_ddpg.launch

#### SAC
	roslaunch training_system test_pretrained_model_sac.launch
		
#### PPO
	roslaunch training_system test_pretrained_model_ppo.launch

#### Note: 
In training and testing change the erp value to 0.5 in Gazebo at the beginning after launching step II. The erp value can be found in the left coloumn in World settings then Physics then constraints then erp.

#### Results

![10 degrees ramp](results/sac_4ramps.gif "10 degrees")
![Varying inclination ramp](results/sac_wavy.gif "Varying inclination")

### References:
[1] Haarnoja, T., Zhou, A., Abbeel, P. & Levine, S., “Soft Actor-Critic: Off-Policy Maximum Entropy Deep Reinforcement Learning with a Stochastic Actor”. Int. Conf. on Machine Learning, 2018.

[2] Schulman, J., Wolski, F., Dhariwal, P., Radford, A., and Klimov, O., “Proximal Policy Optimization Algorithms.” ArXiv abs/1707.06347 (2017).

[3] Maxim Lapan., Deep Reinforcement Learning Hands-On, (2nd ed.) Packt Publishing, 2020.

[4] Lillicrap, Timothy & Hunt, Jonathan & Pritzel, Alexander & Heess, Nicolas & Erez, Tom & Tassa, Yuval & Silver, David & Wierstra, Daan, “Continuous control with deep reinforcement learning, CoRR,” arXiv:1509.02971, 2015.

[5] Silver, David & Lever, Guy & Heess, Nicolas & Degris, Thomas & Wierstra, Daan & Riedmiller, Martin, “Deterministic Policy Gradient Algorithms,” 31st Int. Conference on Machine Learning, ICML 2014.

[6] Spinningup  https://spinningup.openai.com/