#!/usr/bin/env python3

"""Deep Determinitic Policy Gradient (DDPG)
1) Based on  Max Lapan's implementation in Python in his book Deep Reinforcement Learning Hands-On:
https://github.com/PacktPublishing/Deep-Reinforcement-Learning-Hands-On

2) Using ROS and Gazebo 
3) Using OpenAI Gym 
4) Using Pytorch 
5) Usin PTAN (PyTorch AgentNet) package for RL 
https://github.com/Shmuma/ptan


The work presented is a cooperation between University Duisburg-Essen and 
National Technical University of Athens (NTUA).In the scope of a Master Thesis
at the University Duisburg-Essen by Yehia El-Bahrawy under the supervision of
Athanasios Mastrogeorgiou. 
"""

import os
from comet_ml import OfflineExperiment # offline logging
# from comet_ml import Experiment # online logging

import ptan
import time
from datetime import datetime
import gym

import numpy as np

import ddpg_model

import torch
import torch.optim as optim
import torch.nn.functional as F
import adabound
import sys 
print("training python version",sys.version)

import laelaps_env_ellipse

ENV_ID = "LaelapsEnvEllipse-v0"
print("Environment ID",ENV_ID)

import rospy
rospy.init_node('ddpg_laelaps_RL_ellipse', anonymous=True, log_level=rospy.INFO)
# Get Config Parameters from Yaml file ddpg_ellipse.yaml

batch_size = rospy.get_param("/batch_size")
learning_rate_actor = rospy.get_param("/learning_rate_actor")
learning_rate_critic = rospy.get_param("/learning_rate_critic")
optimizer=rospy.get_param("/optimizer")
replay_size = rospy.get_param("/replay_size")
replay_initial = rospy.get_param("/replay_initial")
gamma = rospy.get_param("/gamma")
rewards_steps_count=rospy.get_param("/rewards_steps_count")
validation_tests= rospy.get_param("/validation_tests")
validation_steps = rospy.get_param("/validation_steps")

# Define Hyperparameters for visualization in Comet
hyperparameters=dict(
    batch_size=batch_size,
    learning_rate_actor=learning_rate_actor,
    learning_rate_critic=learning_rate_critic,
    hidden_layer1_actor=rospy.get_param("/hidden_layer1_actor"),
    hidden_layer2_actor=rospy.get_param("/hidden_layer2_actor"),
    hidden_layer1_critic=rospy.get_param("/hidden_layer1_critic"),
    hidden_layer2_critic=rospy.get_param("/hidden_layer2_critic"),
    optimizer=optimizer,
    replay_size=replay_size,
    replay_initial=replay_initial,
    replay_buffer=rospy.get_param("/buffer_method"),
    gamma=gamma,
    belman_rollouts=rewards_steps_count,
    exploration=rospy.get_param("/exploration_method"),
    validation_tests=validation_tests,
    validation_steps=validation_steps
)

time_iter_print=1000
val_episodes=0  # at the end of the run total val_episodes = validation_tests (eg.5) * total_validation_steps (every 500 * total running traning frames)

def validation_net(actor_net,critic_net,frames, env, count=validation_tests, device="cpu"):
    global val_episodes
    with torch.no_grad():
        rewards = 0.0
        episode_rewards=0.0
        val_rewards=[]
        steps = 0
        env.savingpath(test_episode_actions,saving_option=False)
        for i in range(count):
            print("Local time test beginning: ",time.ctime(time.time()))
            obs = env.reset()
            while True:
                obs = ptan.agent.float32_preprocessor([obs]).to(device)
                mu = actor_net(obs)
                action = mu.squeeze(dim=0).data.cpu().numpy()

                obs, reward, done, _ = env.step(action)
                rewards += reward #adding all validation episodes rewards during validation giving total rewards in all validation episodes
                episode_rewards+=reward 
                steps += 1 #adding all validation episodes rewards during validation
                if done: 
                    print("Finished validation count: %s reward %s"% (i,episode_rewards))
                    val_rewards.append(episode_rewards) #list of all validation episode rewards (length of validation_tests)
                    experiment.log_metric('Validation Episodes reward',episode_rewards,step=val_episodes)
                    episode_rewards=0.0
                    val_episodes+=1
                    print("Local time after test: ",time.ctime(time.time()))
                    break
    return rewards / count, steps / count

def unpack_batch(batch, device="cpu"):
    with torch.no_grad():
        states, actions, rewards, dones, last_states = [], [], [], [], []
        for exp in batch:
            states.append(exp.state)
            actions.append(exp.action)
            rewards.append(exp.reward)
            dones.append(exp.last_state is None)
            if exp.last_state is None:
                last_states.append(exp.state) # bec u dont want None values in the target network, it is a trick to avoid none due to termination of the episode
                # but then using the dones bool value Q value will be setted to zero to avoid nan in the network as Q value for None (terminated state) is predetermined to have value 0
            else:
                last_states.append(exp.last_state)
        states = ptan.agent.float32_preprocessor(states).to(device)
        actions = ptan.agent.float32_preprocessor(actions).to(device)
        rewards = ptan.agent.float32_preprocessor(rewards).to(device)
        last_states = ptan.agent.float32_preprocessor(last_states).to(device) 
        dones_t=torch.BoolTensor(dones).to(device)
    return states, actions, rewards, dones_t, last_states

if __name__ == "__main__":
    workspace_name=input("Specifiy the workspace name in comet according to environment (ex. upward): ")
    experiment_name=input("Specifiy the experiment name in comet for running the algorithm: ")
    start_time=time.ctime(time.time())
    print("START TIME: ",start_time)
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    # device = torch.device('cpu')
    
    if torch.cuda.is_available():
        print("I am using Device: GPU" )
    else: 
        print("I am using Device: CPU")

    env = gym.make(ENV_ID)
    print("Training Env init seed:",env.seed_num)
    hyperparameters.update( {'Train Seed' : env.seed_num} )

    validation_env = gym.make(ENV_ID)
    print("Validation Env init seed:",validation_env.seed_num)
    hyperparameters.update( {'Validation Seed' : validation_env.seed_num} )
    input("Change in Gazebo physics concentraints the ERP value to 0.5 then press ENTER!")

    act_net = ddpg_model.DDPGActor(env.observation_space.shape[0], env.action_space.shape[0]).to(device)
    crt_net = ddpg_model.DDPGCritic(env.observation_space.shape[0], env.action_space.shape[0]).to(device)
    print(act_net)
    print(crt_net)
    tgt_act_net = ptan.agent.TargetNet(act_net)
    tgt_crt_net = ptan.agent.TargetNet(crt_net)
    
    #_________________________LOGGING______________________________________________________
    # Creat folder to save tensorboard files depending on test conditions
    # 
    date_now = datetime.now()
    newDirName = date_now.strftime("%B_%d_%y_%H-%M")
    save_folder=os.path.dirname(os.path.realpath(__file__))+"/runs/"+str(newDirName)+"_DDPG_"+workspace_name+"_"+experiment_name
    
    train_folder= save_folder+"/Train"
    test_folder=  save_folder+"/Test"
    model_save=   save_folder+"/Model_best_test_rewards"
    train_episode_actions = train_folder+"/Episode_actions"
    test_episode_actions =  test_folder+"/Episode_actions"

    os.makedirs(save_folder) # Main folder
    os.makedirs(train_folder) # Train folder
    os.makedirs(test_folder) # Test folder 
    os.makedirs(model_save)  # Model per test save 
    os.makedirs(train_episode_actions) # Save env values (i.e. actions, torques, etc.) used in each episode
    os.makedirs(test_episode_actions) # Save env values (i.e. actions, torques, etc.) used in each episode
    
    experiment = OfflineExperiment(
        workspace="leggedteam",
        project_name="DDPG_"+workspace_name,
        offline_directory=save_folder,
        log_code=False,
        log_graph=False)
    experiment.set_name(experiment_name)
    experiment.log_parameters(hyperparameters)
    '''
    To upload logged metrics and hyperparameters to comet
    Example: 
    1) export COMET_API_KEY="..."
    2) comet upload /tmp/comet/5da271fcb60b4652a51dfc0decbe7cd9.zip
    '''
    #____________________________________________
    agent = ddpg_model.AgentDDPG(act_net, device=device)
    exp_source = ptan.experience.ExperienceSourceFirstLast(env, agent, gamma=gamma, steps_count=rewards_steps_count)
    """
    ExperienceSourceFirstLast: store tajectories with the first and last states only instead of full trajectory. 
    For every trajectory it outputs exp_source: 
    first states of the episode, and last states of the episode, total discounted rewards, and actions taken in first states 
    """
    buffer = ptan.experience.ExperienceReplayBuffer(exp_source, buffer_size=replay_size) # Circular buffer (FIFO)

    if optimizer == "Adam": 
        # Adam optimizer
        print("Using Adam Optimizer")
        act_opt = optim.Adam(act_net.parameters(), lr=learning_rate_actor) # use different optimizers not to complicate how we deal with gradients
        crt_opt = optim.Adam(crt_net.parameters(), lr=learning_rate_critic)
    elif optimizer == "SGD": 
        # SGD optimizer
        print("Using SGD Optimizer")
        act_opt = optim.SGD(act_net.parameters(), lr=learning_rate_actor, momentum=0.9)
        crt_opt = optim.SGD(crt_net.parameters(), lr=learning_rate_critic, momentum=0.9)
    elif optimizer == "AdaBound": 
        # Adabound optimizer
        print("Using AdaBound Optimizer")
        act_opt = adabound.AdaBound(act_net.parameters(), lr=learning_rate_actor, final_lr=0.1) #final lr is for SGD at the end, it is suggested that 0.1 does not affect the results and have in general good behaviour
        crt_opt = adabound.AdaBound(crt_net.parameters(), lr=learning_rate_critic, final_lr=0.1)
    

    with experiment.train():
        total_rewards=[]
        total_steps=[]
        episode_counter=0.0
        ts = time.time()
        ts_frame = 0
        frames = 0
        experiment.set_step(frames)
        while True:
            env.savingpath(train_episode_actions,saving_option=False) 
            frames += 1
            buffer.populate(1) #fills samples into buffer, here one sample
            
            rewards_steps = exp_source.pop_rewards_steps() # 
            if rewards_steps: # Episode is terminated
                episode_counter+=1
                ts_diff = time.time() - ts
                speed = (frames - ts_frame) / ts_diff # Speed of episode computations frames/sec
                rewards, steps = zip(*rewards_steps) #total steps reward per episode
                total_rewards.append(rewards[0])
                total_steps.append(steps[0])
                mean_reward= np.mean(total_rewards[-100:])
                mean_total_steps=np.mean(total_steps[-10:])
                experiment.log_metric('Episode rewards',rewards[0], step=frames)
                experiment.log_metric('Mean rewards',mean_reward, step=frames)
                experiment.log_metric('Mean episode steps',mean_total_steps, step=frames)
                experiment.log_metric('Episodes',episode_counter)
                experiment.log_metric('Episode speed f/s',speed)
                ts_frame = frames
                ts = time.time()
            if frames % 100 ==0:
                print("%s Steps done, %s Episodes, mean reward: %s at: %s f/s"%(frames,int(episode_counter),round(mean_reward,3),round(speed,2)))
            if len(buffer) < replay_initial: #replay initial to start training 
                continue
            # In every iteration store the experience into the replay buffer and sample randomly the training batch
            batch = buffer.sample(batch_size)

            states, actions, rewards, dones, last_states = unpack_batch(batch, device) # first states of the episode, and last states of the episode, total immidiate rewards, and actions taken in first states 
            
            # train critic
            crt_opt.zero_grad()
            q = crt_net(states, actions) #batch actions comes from agent inside the experience by running actor network
            last_act = tgt_act_net.target_model(last_states)
            q_last = tgt_crt_net.target_model(last_states, last_act)
            q_last[dones] = 0.0 # in the dones vector the index of the element with True changes the q_last to 0.0 
            q_ref = rewards.unsqueeze(dim=-1) + q_last * gamma #Bellman equation using target network Q* output
            critic_loss = F.mse_loss(q, q_ref.detach()) #Bellman Error
            critic_loss.backward()
            crt_opt.step()
            experiment.log_metric('critic loss',critic_loss.cpu().detach().numpy(), step=frames)
            experiment.log_metric('Y_target',q_ref.mean().cpu().detach().numpy(), step=frames)

            # train actor
            act_opt.zero_grad()
            cur_actions = act_net(states)
            actor_loss = -crt_net(states, cur_actions)
            actor_loss = actor_loss.mean()
            actor_loss.backward()
            act_opt.step()
            experiment.log_metric('actor loss',actor_loss.cpu().detach().numpy(), step=frames)
            
            # Target networks update
            # soft sync (better for continuous action space) the weights of the optimized network to the target network on every step, 
            # but only small ratio of the optimized network weights are transfered to the target network
            tgt_act_net.alpha_sync(alpha=1 - 1e-3)
            tgt_crt_net.alpha_sync(alpha=1 - 1e-3)
            
            # Validation learning 
            with experiment.validate():
                experiment.set_step(frames)
                if frames % validation_steps == 0:
                    print("START TIME: ",start_time)
                    if frames % time_iter_print ==0:
                        local_time_now=time.ctime(time.time())
                        print("Local time now: ",local_time_now)
                    ts = time.time()
                    rewards, steps = validation_net(act_net,crt_net,frames, validation_env, device=device)
                    print("Validation done in %.2f sec, reward %.3f, steps %d" % (
                        time.time() - ts, rewards, steps))
                    experiment.log_metric('Mean Validation rewards',rewards, step=frames)  # the sum of the reward happened until termination (one episode) in every step divided by how many the model is validated
                    experiment.log_metric('Mean Validation steps',steps, step=frames) # the sum of test episodes steps divided by how many the model is validated
                    experiment.log_metric('Episodes_Val',episode_counter) #episode of training where the validation net starts
                    name = "Validation_Model_%+.3f_%d.dat" % (rewards, frames)
                    fname = os.path.join(model_save, name)
                    torch.save(act_net.state_dict(), fname) #save validation network parameters

