#!/usr/bin/env python3

"""Proximal Policy Optimization (PPO)
1) Based on  Max Lapan's implementation in Python in his book Deep Reinforcement Learning Hands-On:
https://github.com/PacktPublishing/Deep-Reinforcement-Learning-Hands-On

2) Using ROS and Gazebo 
3) Using OpenAI Gym 
4) Using Pytorch 
5) Usin PTAN (PyTorch AgentNet) package for RL 
https://github.com/Shmuma/ptan


The work presented is a cooperation between University Duisburg-Essen and 
National Technical University of Athens (NTUA).In the scope of a Master Thesis
at the University Duisburg-Essen by Yehia El-Bahrawy under the supervision of
Athanasios Mastrogeorgiou. 
"""
#  logs metrics to comet.ml

import os
from comet_ml import OfflineExperiment # offline logging
# from comet_ml import Experiment # online logging

import ptan
import time
from datetime import datetime
import gym

import numpy as np
import math

import ppo_model

import torch
import torch.optim as optim
import torch.nn.functional as F
import adabound
import sys 
print("training python version",sys.version)

import laelaps_env_ellipse

ENV_ID = "LaelapsEnvEllipse-v0"
print("Environment ID",ENV_ID)
print("BOOK Version")
import rospy
rospy.init_node('ppo_laelaps_RL_ellipse', anonymous=True, log_level=rospy.INFO)

# Get Config Parameters from Yaml file 

batch_size = rospy.get_param("/batch_size")
learning_rate_actor = rospy.get_param("/learning_rate_actor")
learning_rate_critic = rospy.get_param("/learning_rate_critic")
optimizer=rospy.get_param("/optimizer")
trajectory_size = rospy.get_param("/trajectory_size") 
gamma = rospy.get_param("/gamma")
adv_lambda = rospy.get_param("/advantage_lambda") # constant that specifies the lambda factor in the advantage estimator,  Factor for trade-off of bias vs variance for Generalized Advantage Estimator
eps = rospy.get_param("/eps") # for clipping the policy gradient objective, clipping serves as a regularizer by removing incentives for the policy to change dramatically, and the hyperparameter \epsilon corresponds to how far away the new policy can go from the old while still profiting the objective.
epochs = rospy.get_param("/epochs") # The PPO method also uses a slightly different training procedure: a long sequence of samples is obtained from the environment and then the advantage is estimated for the whole sequence before several epochs of training are performed.
validation_tests= rospy.get_param("/validation_tests")
validation_steps = rospy.get_param("/validation_steps")
rewards_steps_count=rospy.get_param("/rewards_steps_count")

# Define Hyperparameters for visualization in Comet
hyperparameters=dict(
    batch_size=batch_size,
    learning_rate_actor=learning_rate_actor,
    learning_rate_critic=learning_rate_critic,
    hidden_layer1_actor=rospy.get_param("/hidden_layer1_actor"),
    hidden_layer2_actor=rospy.get_param("/hidden_layer2_actor"),
    hidden_layer1_critic=rospy.get_param("/hidden_layer1_critic"),
    hidden_layer2_critic=rospy.get_param("/hidden_layer2_critic"),
    optimizer=optimizer,
    trajectory_size=trajectory_size,
    gamma=gamma,
    adv_lambda=adv_lambda,
    eps=eps,
    epochs=epochs,
    belman_rollouts=rewards_steps_count,
    validation_tests=validation_tests,
    validation_steps=validation_steps
)
"""
Spinnup
It is still possible to end up with a new policy which is too far from the old policy, and there are a bunch of tricks used by different PPO implementations to stave this off.
In our implementation here, we use a particularly simple method: early stopping. If the mean KL-divergence of the new policy from the old grows beyond a threshold, we stop taking gradient steps.

Exploration vs Exploitation
PPO trains a stochastic policy in an on-policy way. This means that it explores by sampling actions according to the latest version of its stochastic policy. The amount of randomness in action 
selection depends on both initial conditions and the training procedure. Over the course of training, the policy typically becomes progressively less random, as the update rule encourages it to 
exploit rewards that it has already found. This may cause the policy to get trapped in local optima.
"""
time_iter_print=1000
val_episodes=0
def validation_net(actor_net,critic_net,frames, env, count=validation_tests, device="cpu"):
    global val_episodes
    rewards = 0.0
    episode_rewards=0.0
    val_rewards=[]
    steps = 0
    env.savingpath(test_episode_actions,saving_option=False)
    for i in range(count):
        print("Local time test beginning: ",time.ctime(time.time()))
        obs = env.reset()
        while True:
            obs = ptan.agent.float32_preprocessor([obs]).to(device)
            mu = actor_net(obs)
            action = mu.squeeze(dim=0).data.cpu().numpy()

            obs, reward, done, _ = env.step(action)
            rewards += reward #adding all validation episodes rewards during validation giving total rewards in all validation episodes
            episode_rewards+=reward 
            steps += 1 #adding all validation episodes rewards during validation
            if done: 
                print("Finished validation count: %s reward %s"% (i,episode_rewards))
                val_rewards.append(episode_rewards) #list of all validation episode rewards (length of validation_tests)
                experiment.log_metric('Validation Episodes reward',episode_rewards,step=val_episodes)
                episode_rewards=0.0
                val_episodes+=1
                print("Local time after test: ",time.ctime(time.time()))
                break
    return rewards / count, steps / count 

def calc_logprob(mu_v, logstd_v, actions_v):
    p1 = - ((mu_v - actions_v) ** 2) / (2*torch.exp(logstd_v).clamp(min=1e-3))
    p2 = - torch.log(torch.sqrt(2 * math.pi * torch.exp(logstd_v)))
    return p1 + p2

def calc_adv_ref(trajectory, net_crt, states_v, device="cpu"):
    values_v = net_crt(states_v)
    values = values_v.squeeze().data.cpu().numpy()
    # generalized advantage estimator: smoothed version of the advantage
    last_lambda = 0.0
    result_adv = []
    result_ref = []
    for val, next_val, (exp,) in zip(reversed(values[:-1]),
                                    reversed(values[1:]),
                                    reversed(trajectory[:-1])):
        if exp.done:
            delta = exp.reward - val
            last_lambda = delta
        else:
            delta = exp.reward + gamma * next_val - val
            last_lambda = delta + gamma * adv_lambda * last_lambda
        result_adv.append(last_lambda)
        result_ref.append(last_lambda + val)

    adv_v = torch.FloatTensor(list(reversed(result_adv)))
    ref_v = torch.FloatTensor(list(reversed(result_ref)))
    return adv_v.to(device), ref_v.to(device)



if __name__ == "__main__":
    workspace_name=input("Specifiy the workspace name in comet according to environment (ex. upward): ")
    experiment_name=input("Specifiy the experiment name in comet for running the algorithm: ")
    start_time=time.ctime(time.time())
    print("START TIME: ",start_time)
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    # device = torch.device('cpu')
    
    if torch.cuda.is_available():
        print("I am using Device: GPU" )
    else: 
        print("I am using Device: CPU")

    env = gym.make(ENV_ID)
    print("Training Env init seed:",env.seed_num)
    hyperparameters.update( {'Train Seed' : env.seed_num} )

    validation_env = gym.make(ENV_ID)
    print("Validation Env init seed:",validation_env.seed_num)
    hyperparameters.update( {'Validation Seed' : validation_env.seed_num} )
    input("Change in Gazebo physics concentraints the ERP value to 0.5 then press ENTER!")

    act_net = ppo_model.PPOActor(env.observation_space.shape[0], env.action_space.shape[0]).to(device)
    crt_net = ppo_model.PPOCritic(env.observation_space.shape[0]).to(device)
    print(act_net)
    print(crt_net)


    #_________________________LOGGING______________________________________________________
    # Creat folder to save tensorboard files depending on test conditions
    # 
    date_now = datetime.now()
    newDirName = date_now.strftime("%B_%d_%y_%H-%M")
    save_folder=os.path.dirname(os.path.realpath(__file__))+"/runs/"+str(newDirName)+"_PPO_"+workspace_name+"_"+experiment_name
    
    train_folder= save_folder+"/Train"
    test_folder=  save_folder+"/Test"
    model_save=   save_folder+"/Model_best_test_rewards"
    train_episode_actions = train_folder+"/Episode_actions"
    test_episode_actions =  test_folder+"/Episode_actions"

    os.makedirs(save_folder) # Main folder
    os.makedirs(train_folder) # Train folder
    os.makedirs(test_folder) # Test folder 
    os.makedirs(model_save)  # Model per test save 
    os.makedirs(train_episode_actions) # Save env values (i.e. actions, torques, etc.) used in each episode
    os.makedirs(test_episode_actions) # Save env values (i.e. actions, torques, etc.) used in each episode
    
    experiment = OfflineExperiment(
        workspace="leggedteam",
        project_name="PPO_"+workspace_name,
        offline_directory=save_folder,
        log_code=False,
        log_graph=False)
    experiment.set_name(experiment_name)
    experiment.log_parameters(hyperparameters)
    '''
    To upload logged metrics and hyperparameters to comet
    Example: 
    1) export COMET_API_KEY="..."
    2) comet upload /tmp/comet/5da271fcb60b4652a51dfc0decbe7cd9.zip
    '''
    #____________________________________________
    agent = ppo_model.AgentPPO(act_net, device=device)
    exp_source = ptan.experience.ExperienceSource(env, agent, steps_count=rewards_steps_count)

    if optimizer == "Adam": 
        # Adam optimizer
        print("Using Adam Optimizer")
        act_opt = optim.Adam(act_net.parameters(), lr=learning_rate_actor) # use different optimizers not to complicate how we deal with gradients
        crt_opt = optim.Adam(crt_net.parameters(), lr=learning_rate_critic)
    elif optimizer == "SGD": 
        # SGD optimizer
        print("Using SGD Optimizer")
        act_opt = optim.SGD(act_net.parameters(), lr=learning_rate_actor, momentum=0.9)
        crt_opt = optim.SGD(crt_net.parameters(), lr=learning_rate_critic, momentum=0.9)
    elif optimizer == "AdaBound": 
        # Adabound optimizer
        print("Using AdaBound Optimizer")
        act_opt = adabound.AdaBound(act_net.parameters(), lr=learning_rate_actor, final_lr=0.1) #final lr is for SGD at the end, it is suggested that 0.1 does not affect the results and have in general good behaviour
        crt_opt = adabound.AdaBound(crt_net.parameters(), lr=learning_rate_critic, final_lr=0.1)
    

    with experiment.train():
        total_rewards=[]
        total_steps=[]
        episode_counter=0.0
        ts = time.time()
        ts_frame = 0
        mean_reward=0
        speed=0
        trajectory = []
        for frames, exp in enumerate(exp_source):
            experiment.set_step(frames)
            env.savingpath(train_episode_actions,saving_option=False) 
            rewards_steps = exp_source.pop_rewards_steps() # 
            if rewards_steps: # Episode is terminated
                episode_counter+=1
                ts_diff = time.time() - ts
                speed = (frames - ts_frame) / ts_diff # Speed of episode computations frames/sec
                rewards, steps = zip(*rewards_steps) #total steps reward per episode
                total_rewards.append(rewards[0])
                total_steps.append(steps[0])
                mean_reward= np.mean(total_rewards[-100:])
                mean_total_steps=np.mean(total_steps[-10:])
                experiment.log_metric('Episode rewards',rewards[0], step=frames)
                experiment.log_metric('Mean rewards',mean_reward, step=frames)
                experiment.log_metric('Mean episode steps',mean_total_steps, step=frames)
                experiment.log_metric('Episodes',episode_counter)
                experiment.log_metric('Episode speed f/s',speed)
                ts_frame = frames
                ts = time.time()
            if frames % 100 ==0:
                print("%s Steps done, %s Episodes, mean reward: %s at: %s f/s"%(frames,int(episode_counter),round(mean_reward,3),round(speed,2)))

            # Validation learning 
            if frames > 0 and frames % validation_steps == 0:
                with experiment.validate():
                    experiment.set_step(frames)
                    print("START TIME: ",start_time)
                    if frames % time_iter_print ==0:
                        local_time_now=time.ctime(time.time())
                        print("Local time now: ",local_time_now)
                    ts = time.time()
                    rewards, steps = validation_net(act_net,crt_net,frames, validation_env, device=device)
                    print("Validation done in %.2f sec, reward %.3f, steps %d" % (
                        time.time() - ts, rewards, steps))
                    experiment.log_metric('Mean Validation rewards',rewards, step=frames)  # the sum of the reward happened until termination (one episode) in every step divided by how many the model is validated
                    experiment.log_metric('Mean Validation steps',steps, step=frames) # the sum of test episodes steps divided by how many the model is validated
                    experiment.log_metric('Episodes_Val',episode_counter) #episode of training where the validation net starts
                    name = "Validation_Model_%+.3f_%d.dat" % (rewards, frames)
                    fname = os.path.join(model_save, name)
                    torch.save(act_net.state_dict(), fname) #save validation network parameters

            trajectory.append(exp)
            if len(trajectory) < trajectory_size:
                continue
            print("Trajectory is full")
            traj_states = [t[0].state for t in trajectory]
            traj_actions = [t[0].action for t in trajectory]
            traj_states_v = torch.FloatTensor(traj_states)
            traj_states_v = traj_states_v.to(device)
            traj_actions_v = torch.FloatTensor(traj_actions)
            traj_actions_v = traj_actions_v.to(device)
            traj_adv_v, traj_ref_v = calc_adv_ref(
                trajectory, crt_net, traj_states_v, device=device)
            mu_v = act_net(traj_states_v)
            old_logprob_v = calc_logprob(mu_v, act_net.logstd, traj_actions_v)

            # normalize advantages: Using the normalized advantage introduces a bias to the policy gradient estimator, but it reduces variance a lot.
            traj_adv_v = traj_adv_v - torch.mean(traj_adv_v)
            traj_adv_v /= torch.std(traj_adv_v)

            # drop last entry from the trajectory, an our adv and ref value calculated without it
            trajectory = trajectory[:-1]
            old_logprob_v = old_logprob_v[:-1].detach()

            sum_loss_value = 0.0
            sum_loss_policy = 0.0
            count_steps = 0

            for epoch in range(epochs):
                for batch_ofs in range(0, len(trajectory),
                                        batch_size):
                    # print("Training") # loop of 160 times
                    batch_l = batch_ofs + batch_size
                    states_v = traj_states_v[batch_ofs:batch_l]
                    actions_v = traj_actions_v[batch_ofs:batch_l]
                    batch_adv_v = traj_adv_v[batch_ofs:batch_l]
                    batch_adv_v = batch_adv_v.unsqueeze(-1)
                    batch_ref_v = traj_ref_v[batch_ofs:batch_l]
                    batch_old_logprob_v = \
                        old_logprob_v[batch_ofs:batch_l]

                    # critic training
                    crt_opt.zero_grad()
                    value_v = crt_net(states_v)
                    loss_value_v = F.mse_loss(
                        value_v.squeeze(-1), batch_ref_v)
                    loss_value_v.backward()
                    crt_opt.step()

                    # actor training
                    act_opt.zero_grad()
                    mu_v = act_net(states_v)
                    logprob_pi_v = calc_logprob(
                        mu_v, act_net.logstd, actions_v)
                    ratio_v = torch.exp(
                        logprob_pi_v - batch_old_logprob_v)
                    surr_obj_v = batch_adv_v * ratio_v
                    c_ratio_v = torch.clamp(ratio_v,
                                            1.0 - eps,
                                            1.0 + eps)
                    clipped_surr_v = batch_adv_v * c_ratio_v
                    loss_policy_v = -torch.min(
                        surr_obj_v, clipped_surr_v).mean()
                    loss_policy_v.backward()
                    act_opt.step()

                    sum_loss_value += loss_value_v.item()
                    sum_loss_policy += loss_policy_v.item()
                    count_steps += 1
            print("Training finished mean loss policy",sum_loss_policy / count_steps)
            print("Training finished mean loss value",sum_loss_value / count_steps)
            trajectory.clear()
            experiment.log_metric('Advantage',traj_adv_v.mean().item(), step=frames)
            experiment.log_metric('values',traj_ref_v.mean().item(), step=frames)
            experiment.log_metric('loss_policy',sum_loss_policy / count_steps, step=frames)
            experiment.log_metric('loss_value',sum_loss_value / count_steps, step=frames)



