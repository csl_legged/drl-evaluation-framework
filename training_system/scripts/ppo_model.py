"""Proximal Policy Optimization (PPO) Network & Agent
1) Based on  Max Lapan's implementation in Python in his book Deep Reinforcement Learning Hands-On:
https://github.com/PacktPublishing/Deep-Reinforcement-Learning-Hands-On

2) Using Pytorch 
3) Usin PTAN (PyTorch AgentNet) package for RL 
https://github.com/Shmuma/ptan
"""


import ptan
import numpy as np
import rospy

import torch
import torch.nn as nn
import torch.nn.functional as F

hidden_layer1_actor = rospy.get_param("/hidden_layer1_actor")
hidden_layer2_actor = rospy.get_param("/hidden_layer2_actor")

hidden_layer1_critic = rospy.get_param("/hidden_layer1_critic")
hidden_layer2_critic = rospy.get_param("/hidden_layer2_critic")


class PPOActor(nn.Module):
    def __init__(self, obs_size, act_size):
        super(PPOActor, self).__init__()

        self.mu = nn.Sequential(
            nn.Linear(obs_size, hidden_layer1_actor),
            nn.Tanh(),
            nn.Linear(hidden_layer1_actor, hidden_layer2_actor),
            nn.Tanh(),
            nn.Linear(hidden_layer2_critic, act_size),
            nn.Tanh(),
        )
        self.logstd = nn.Parameter(torch.zeros(act_size)) # log standard deviation depends on weights and biases of mu network but not parameterized with the state

    def forward(self, x):
        return self.mu(x)

"""
Note: 
logstd is parameter of the actor network
having a state-independent std is like having a global exploration schedule 
(explore a lot at the beginning and become more and more deterministic over training).
It is true that without the entropy bonus, the std can decrease quite fast, leading to early convergence.
In practice, this factor is usually set to zero and at the end we use the deterministic policy 
for testing (for continuous actions environment).
"""

class PPOCritic(nn.Module):
    def __init__(self, obs_size):
        super(PPOCritic, self).__init__()

        self.value = nn.Sequential(
            nn.Linear(obs_size, hidden_layer1_critic),
            nn.ReLU(),
            nn.Linear(hidden_layer1_critic, hidden_layer2_critic),
            nn.ReLU(),
            nn.Linear(hidden_layer2_critic, 1),
        )

    def forward(self, x):
        return self.value(x)

class AgentPPO(ptan.agent.BaseAgent):
    def __init__(self, net, device="cpu"):
        self.net = net
        self.device = device

    @torch.no_grad()
    def __call__(self, states, agent_states):
        states_v = ptan.agent.float32_preprocessor(states)
        states_v = states_v.to(self.device)

        mu_v = self.net(states_v)
        mu = mu_v.data.cpu().numpy()
        logstd = self.net.logstd.data.cpu().numpy()
        rnd = np.random.normal(size=logstd.shape)
        actions = mu + np.exp(logstd) * rnd
        actions = np.clip(actions, -1, 1)
        return actions, agent_states


