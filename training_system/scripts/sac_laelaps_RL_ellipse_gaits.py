#!/usr/bin/env python3

"""Soft Actor-Critic (SAC)
1) Based on  Max Lapan's implementation in Python in his book Deep Reinforcement Learning Hands-On:
https://github.com/PacktPublishing/Deep-Reinforcement-Learning-Hands-On
&
Spinningup Baseline
https://spinningup.openai.com/en/latest/algorithms/sac.html

2) Using ROS and Gazebo 
3) Using OpenAI Gym 
4) Using Pytorch 
5) Usin PTAN (PyTorch AgentNet) package for RL 
https://github.com/Shmuma/ptan


The work presented is a cooperation between University Duisburg-Essen and 
National Technical University of Athens (NTUA).In the scope of a Master Thesis
at the University Duisburg-Essen by Yehia El-Bahrawy under the supervision of
Athanasios Mastrogeorgiou. 
"""


import os
from comet_ml import OfflineExperiment # offline logging
# from comet_ml import Experiment # online logging

import ptan
import time
from datetime import datetime
import gym

import numpy as np

import sac_model

import torch
import torch.optim as optim
import torch.nn.functional as F
import torch.distributions as distrib
import adabound
import sys 
print("training python version",sys.version)

import laelaps_env_ellipse

ENV_ID = "LaelapsEnvEllipse-v0"
print("Environment ID",ENV_ID)

import rospy
rospy.init_node('sac_laelaps_RL_ellipse', anonymous=True, log_level=rospy.INFO)

# Get Config Parameters from Yaml file 

batch_size = rospy.get_param("/batch_size")
learning_rate_actor = rospy.get_param("/learning_rate_actor")
learning_rate_critic = rospy.get_param("/learning_rate_critic")
learning_rate_twinQ=rospy.get_param("/learning_rate_twinQ")
entropy_alpha=rospy.get_param("/entropy_alpha")
optimizer=rospy.get_param("/optimizer")
replay_size = rospy.get_param("/replay_size")
replay_initial = rospy.get_param("/replay_initial")
gamma = rospy.get_param("/gamma")
rewards_steps_count=rospy.get_param("/rewards_steps_count")
validation_tests= rospy.get_param("/validation_tests")
validation_steps = rospy.get_param("/validation_steps")

# Define Hyperparameters for visualization in Comet
hyperparameters=dict(
    batch_size=batch_size,
    learning_rate_actor=learning_rate_actor,
    learning_rate_critic=learning_rate_critic,
    learning_rate_twinQ=learning_rate_twinQ,
    hidden_layer1_actor=rospy.get_param("/hidden_layer1_actor"),
    hidden_layer2_actor=rospy.get_param("/hidden_layer2_actor"),
    hidden_layer1_critic=rospy.get_param("/hidden_layer1_critic"),
    hidden_layer2_critic=rospy.get_param("/hidden_layer2_critic"),
    hidden_layer1_twinQ=rospy.get_param("/hidden_layer1_twinQ"),
    hidden_layer2_twinQ=rospy.get_param("/hidden_layer2_twinQ"),
    entropy_alpha=entropy_alpha,
    optimizer=optimizer,
    replay_size=replay_size,
    replay_initial=replay_initial,
    replay_buffer=rospy.get_param("/buffer_method"),
    gamma=gamma,
    belman_rollouts=rewards_steps_count,
    exploration=rospy.get_param("/exploration_method"),
    validation_tests=validation_tests,
    validation_steps=validation_steps
)

time_iter_print=1000
val_episodes=0
def validation_net(actor_net,critic_net,frames, env, count=validation_tests, device="cpu"):
    global val_episodes
    with torch.no_grad():
        rewards = 0.0
        episode_rewards=0.0
        val_rewards=[]
        steps = 0
        env.savingpath(test_episode_actions,saving_option=False)

        for i in range(count):
            print("Local time test beginning: ",time.ctime(time.time()))
            obs = env.reset()
            while True:
                obs = ptan.agent.float32_preprocessor([obs]).to(device)
                mu,_ = actor_net(obs,deterministic=True,with_logprob=False) # action equal to mean value from the network (like DDPG)
                action = mu.squeeze(dim=0).data.cpu().numpy()
                obs, reward, done, _ = env.step(action)
                rewards += reward #adding all validation episodes rewards during validation giving total rewards in all validation episodes
                episode_rewards+=reward 
                steps += 1 #adding all validation episodes rewards during validation
                if done: 
                    print("Finished validation count: %s reward %s"% (i,episode_rewards))
                    val_rewards.append(episode_rewards) #list of all validation episode rewards (length of validation_tests)
                    experiment.log_metric('Validation Episodes reward',episode_rewards,step=val_episodes)
                    episode_rewards=0.0
                    val_episodes+=1
                    print("Local time after test: ",time.ctime(time.time()))
                    break
    return rewards / count, steps / count 

def unpack_batch(batch, target_net, last_val_gamma, device="cpu"):
    """
    Convert batch into training tensors
    :param batch:
    :param net:
    :return: states variable, actions tensor, reference values variable
    """
    states = []
    actions = []
    rewards = []
    not_done_idx = []
    last_states = []
    for idx, exp in enumerate(batch):
        states.append(exp.state)
        actions.append(exp.action)
        rewards.append(exp.reward)
        if exp.last_state is not None:
            not_done_idx.append(idx)
            last_states.append(exp.last_state)
    states = ptan.agent.float32_preprocessor(states).to(device)
    actions = torch.FloatTensor(actions).to(device)
    rewards_np = np.array(rewards, dtype=np.float32)
    if not_done_idx:
        last_states = ptan.agent.float32_preprocessor(last_states).to(device)
        last_vals = target_net(last_states) # Get from the target network the next state the state value function V'
        last_vals_np = last_vals.data.cpu().numpy()[:, 0]
        rewards_np[not_done_idx] += last_val_gamma * last_vals_np

    y_q = torch.FloatTensor(rewards_np).to(device) # y_q of the target from belleman equation
    return states, actions, y_q

"""
torch.no_grad() : will make all the operations in the block have no gradients. 
which means you prevent tracking history and using memory, where the tensors may
have trainable parameters with requires_grad=True, but for changing the values no 
gradient part needs to be changed. The gradient information from before will be 
the same and backpropagation will not be affected.
"""
def unpack_batch_sac(batch, val_net, twinq_net, policy_net,gamma: float, ent_alpha: float,device="cpu"):
    """
    Unpack Soft Actor-Critic batch
    """
    with torch.no_grad():
        states, actions, y_q = unpack_batch(batch, val_net, gamma, device) #states and actions from batch, y_q: target for Q value function for calculating Bellman MSE loss
        """
        calculate the reference for the V-network from the minimum of the twin
        Q-values minus the scaled entropy coefficient. The entropy is calculated from our
        current policy network.
        """
        # references for the critic network
        act, logpi = policy_net(states)
        q1, q2 = twinq_net(states, act) # act: mean of the policy distribution actions goes to the twin Q network
        # element-wise minimum
        ref_v = torch.min(q1, q2) - ent_alpha * logpi #ref V value function to calculate critic loss 
    return states, actions, ref_v, y_q


if __name__ == "__main__":
    workspace_name=input("Specifiy the workspace name in comet according to environment (ex. upward): ")
    experiment_name=input("Specifiy the experiment name in comet for running the algorithm: ")
    start_time=time.ctime(time.time())
    print("START TIME: ",start_time)
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    # device = torch.device('cpu')
    
    if torch.cuda.is_available():
        print("I am using Device: GPU" )
    else: 
        print("I am using Device: CPU")

    env = gym.make(ENV_ID)
    print("Training Env init seed:",env.seed_num)
    hyperparameters.update( {'Train Seed' : env.seed_num} )

    validation_env = gym.make(ENV_ID)
    print("Validation Env init seed:",validation_env.seed_num)
    hyperparameters.update( {'Validation Seed' : validation_env.seed_num} )
    input("Change in Gazebo physics concentraints the ERP value to 0.5 then press ENTER!")

    act_net = sac_model.GaussianActor(
        env.observation_space.shape[0],
        env.action_space.shape[0]).to(device)
    crt_net = sac_model.ModelCritic(
        env.observation_space.shape[0]
    ).to(device)
    twinQ_net = sac_model.ModelSACTwinQ(
        env.observation_space.shape[0],
        env.action_space.shape[0]).to(device)
    print("Actor network",act_net)
    print("Critic network",crt_net)
    print("twinQ network",twinQ_net)

    tgt_crt_net = ptan.agent.TargetNet(crt_net)

    #_________________________LOGGING______________________________________________________
    # 
    date_now = datetime.now()
    newDirName = date_now.strftime("%B_%d_%y_%H-%M")
    save_folder=os.path.dirname(os.path.realpath(__file__))+"/runs/"+str(newDirName)+"_SAC_"+workspace_name+"_"+experiment_name
    
    train_folder= save_folder+"/Train"
    test_folder=  save_folder+"/Test"
    model_save=   save_folder+"/Model_best_test_rewards"
    train_episode_actions = train_folder+"/Episode_actions"
    test_episode_actions =  test_folder+"/Episode_actions"

    os.makedirs(save_folder) # Main folder
    os.makedirs(train_folder) # Train folder
    os.makedirs(test_folder) # Test folder 
    os.makedirs(model_save)  # Model per test save 
    os.makedirs(train_episode_actions) # Save env values (i.e. actions, torques, etc.) used in each episode
    os.makedirs(test_episode_actions) # Save env values (i.e. actions, torques, etc.) used in each episode
    
    #  save random seed into hyperparameters used : training_seed=env.seed_num, validation_seed=test_env.seed_num
    experiment = OfflineExperiment(
        workspace="leggedteam",
        project_name="SAC_"+workspace_name,
        offline_directory=save_folder,
        log_code=False,
        log_graph=False)
    experiment.set_name(experiment_name)
    experiment.log_parameters(hyperparameters)

    '''
    To upload logged metrics and hyperparameters to comet
    Example: 
    1) export COMET_API_KEY="..."
    2) comet upload /tmp/comet/5da271fcb60b4652a51dfc0decbe7cd9.zip
    '''
    #___________________________________________
    agent = sac_model.AgentSAC(act_net, device=device)
    exp_source = ptan.experience.ExperienceSourceFirstLast(env, agent, gamma=gamma, steps_count=rewards_steps_count)
    """
    ExperienceSourceFirstLast: store tajectories with the first and last states only instead of full trajectory. 
    For every trajectory it outputs exp_source: 
    first states of the episode, and last states of the episode, total discounted rewards, and actions taken in first states 
    """
    buffer = ptan.experience.ExperienceReplayBuffer(exp_source, buffer_size=replay_size) # Circular buffer (FIFO)
    # do three
    # different optimization steps: for V, for Q, and for the policy.
    if optimizer == "Adam": 
        # Adam optimizer
        print("Using Adam Optimizer")
        act_opt = optim.Adam(act_net.parameters(), lr=learning_rate_actor) # use different optimizers not to complicate how we deal with gradients
        crt_opt = optim.Adam(crt_net.parameters(), lr=learning_rate_critic)
        twinQ_opt = optim.Adam(twinQ_net.parameters(), lr=learning_rate_twinQ)
    elif optimizer == "SGD": 
        # SGD optimizer
        print("Using SGD Optimizer")
        act_opt = optim.SGD(act_net.parameters(), lr=learning_rate_actor, momentum=0.9)
        crt_opt = optim.SGD(crt_net.parameters(), lr=learning_rate_critic, momentum=0.9)
        twinQ_opt=optim.SGD(twinQ_net.parameters(), lr=learning_rate_twinQ)
    elif optimizer == "AdaBound": 
        # Adabound optimizer
        print("Using AdaBound Optimizer")
        act_opt = adabound.AdaBound(act_net.parameters(), lr=learning_rate_actor, final_lr=0.1) #final lr is for SGD at the end, it is suggested that 0.1 does not affect the results and have in general good behaviour
        crt_opt = adabound.AdaBound(crt_net.parameters(), lr=learning_rate_critic, final_lr=0.1)
        twinQ_opt=adabound.AdaBound(twinQ_net.parameters(), lr=learning_rate_twinQ, final_lr=0.1)

    with experiment.train():
        total_rewards=[]
        total_steps=[]
        episode_counter=0.0
        ts = time.time()
        ts_frame = 0
        frames = 0
        experiment.set_step(frames)
        
        while True:
            env.savingpath(train_episode_actions,saving_option=False) 
            frames += 1
            buffer.populate(1) #fills samples into buffer, here one sample
            
            rewards_steps = exp_source.pop_rewards_steps() # 
            if rewards_steps: # Episode is terminated
                episode_counter+=1
                ts_diff = time.time() - ts
                speed = (frames - ts_frame) / ts_diff # Speed of episode computations frames/sec
                rewards, steps = zip(*rewards_steps) #total steps reward per episode
                total_rewards.append(rewards[0])
                total_steps.append(steps[0])
                mean_reward= np.mean(total_rewards[-100:])
                mean_total_steps=np.mean(total_steps[-10:])
                experiment.log_metric('Episode rewards',rewards[0], step=frames)
                experiment.log_metric('Mean rewards',mean_reward, step=frames)
                experiment.log_metric('Mean episode steps',mean_total_steps, step=frames)
                experiment.log_metric('Episodes',episode_counter)
                experiment.log_metric('Episode speed f/s',speed)
                ts_frame = frames
                ts = time.time()
            if frames % 100 ==0:
                print("%s Steps done, %s Episodes, mean reward: %s at: %s f/s"%(frames,int(episode_counter),round(mean_reward,3),round(speed,2)))
            if len(buffer) < replay_initial: #replay initial to start training 
                continue
            
            # In every iteration store the experience into the replay buffer and sample randomly the training batch
            batch = buffer.sample(batch_size)
            states, actions, ref_v, y_q = unpack_batch_sac(
                        batch, tgt_crt_net.target_model,
                        twinQ_net, act_net, gamma,
                        entropy_alpha, device)
            
            experiment.log_metric('Y_v',ref_v.mean().cpu().detach().numpy(), step=frames)
            experiment.log_metric('Y_q',y_q.mean().cpu().detach().numpy(), step=frames)

            # train TwinQ : should be trained before it is used in the actor training
            twinQ_opt.zero_grad()
            q1, q2 = twinQ_net(states, actions)
            q1_loss = F.mse_loss(q1, y_q.detach())
            q2_loss = F.mse_loss(q2, y_q.detach())
            q_loss = q1_loss + q2_loss
            q_loss.backward()
            twinQ_opt.step()
            experiment.log_metric('loss_q1',q1_loss.cpu().detach().numpy(), step=frames)
            experiment.log_metric('loss_q2',q2_loss.cpu().detach().numpy(), step=frames)

            # Critic
            crt_opt.zero_grad()
            v = crt_net(states)
            v_loss = F.mse_loss(torch.squeeze(v,-1), ref_v.detach())
            v_loss.backward()
            crt_opt.step()
            experiment.log_metric('loss_critic',v_loss.cpu().detach().numpy(), step=frames)
            
            # Actor

            act_opt.zero_grad()
            act, logpi = act_net(states)
            q1_pi, q2_pi = twinQ_net(states, act)
            q_pi=torch.min(q1_pi, q2_pi)
            entropy = entropy_alpha * logpi
            act_loss = -(q_pi - entropy).mean()
            act_loss.backward()
            act_opt.step()
            experiment.log_metric('Entropy',entropy.mean().cpu().detach().numpy(), step=frames)
            experiment.log_metric('loss_actor',act_loss.cpu().detach().numpy(), step=frames)

            tgt_crt_net.alpha_sync(alpha=1 - 1e-3)
            
            # Validation learning 
            with experiment.validate():
                experiment.set_step(frames)
                if frames % validation_steps == 0:
                    print("START TIME: ",start_time)
                    if frames % time_iter_print ==0:
                        local_time_now=time.ctime(time.time())
                        print("Local time now: ",local_time_now)
                    ts = time.time()
                    rewards, steps = validation_net(act_net,crt_net,frames, validation_env, device=device)
                    print("Validation done in %.2f sec, reward %.3f, steps %d" % (
                        time.time() - ts, rewards, steps))
                    experiment.log_metric('Mean Validation rewards',rewards, step=frames)  # the sum of the reward happened until termination (one episode) in every step divided by how many the model is validated
                    experiment.log_metric('Mean Validation steps',steps, step=frames) # the sum of test episodes steps divided by how many the model is validated
                    experiment.log_metric('Episodes_Val',episode_counter) #episode of training where the validation net starts
                    name = "Validation_Model_%+.3f_%d.dat" % (rewards, frames)
                    fname = os.path.join(model_save, name)
                    torch.save(act_net.state_dict(), fname) #save validation network parameters

