"""Soft Actor-Critic (SAC) Network & Agent
1) Based on Spinningup Baseline
https://spinningup.openai.com/en/latest/algorithms/sac.html
&
Soft Actor-Critic: Off-Policy Maximum Entropy Deep Reinforcement Learning with a Stochastic Actor
2) Using Pytorch 
3) Usin PTAN (PyTorch AgentNet) package for RL 
https://github.com/Shmuma/ptan
"""


import ptan
import numpy as np
import rospy

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.distributions.normal import Normal

hidden_layer1_actor = rospy.get_param("/hidden_layer1_actor")
hidden_layer2_actor = rospy.get_param("/hidden_layer2_actor")

hidden_layer1_critic = rospy.get_param("/hidden_layer1_critic")
hidden_layer2_critic = rospy.get_param("/hidden_layer2_critic")

hidden_layer1_twinQ=rospy.get_param("/hidden_layer1_twinQ")
hidden_layer2_twinQ=rospy.get_param("/hidden_layer2_twinQ")

replay_initial=rospy.get_param("/replay_initial")

logstd_min = -20
logstd_max= 2

logprob_flag = rospy.get_param("/logprob_flag")
print("logprob_flag",logprob_flag)

class GaussianActor(nn.Module):
    def __init__(self, obs_size, act_size):
        super(GaussianActor, self).__init__()

        self.mu = nn.Sequential(
            nn.Linear(obs_size, hidden_layer1_actor),
            nn.ReLU(),
            nn.Linear(hidden_layer1_actor, hidden_layer2_actor),
            nn.ReLU(),
            nn.Linear(hidden_layer2_actor, act_size),
        )

        self.logstd = nn.Sequential(
            nn.Linear(obs_size, hidden_layer1_actor),
            nn.ReLU(),
            nn.Linear(hidden_layer1_actor, hidden_layer2_actor),
            nn.ReLU(),
            nn.Linear(hidden_layer2_actor, act_size),
        )
    def forward(self, obs,deterministic=False,with_logprob=logprob_flag):
        mu=self.mu(obs) # mean network 
        logstd=self.logstd(obs) # variance network parameterized by the observation
        logstd=torch.clamp(self.logstd(obs), logstd_min,logstd_max) # logstd_min and max should have sane values, clamping helps to avoid NaN values
        # print("Logstd after clamp",logstd)
        std=torch.exp(logstd)
        # print("std",std)
        # In practice we would sample an action from the output of a network, apply this action in an environment,
        # and then use log_prob to construct an equivalent loss function. 
        # before squashing with tanh function get distribution and sample
        pi_distrib =Normal(mu,std) # pi for actor policy, then sample the normal distribution to inc the exploration probability
        
        if deterministic: # used only for the evaluation steps and testing pre-trained model
            action= mu # action from pi policy
        else: 
            action= pi_distrib.rsample() # reparametrization trick

        if with_logprob:
            # Compute the logprob 
            # from equation 21 in Paper : Soft Actor-Critic: Off-Policy Maximum Entropy Deep Reinforcement Learning with a Stochastic Actor.
            # Note computing the log_prob (log-likelihood) can be a bit expensive. The equations from baseline Spinnup 
            # 
            logp_pi = pi_distrib.log_prob(action).sum(axis=-1)
            logp_pi -= (2*(np.log(2) - action - F.softplus(-2*action))).sum(axis=1)
        else: 
            logp_pi=None

        # Squash the action using the tanh function
        action=torch.tanh(action) # for training it is stochastic action from reparameterization and for evaluation 

        return action,logp_pi

"""
Note the use of clamp, it's super important because otherwise you might
have negative values as the standard deviation and end up having nans,
by using this little trick we ensure we always have a positive std.
"""

class ModelCritic(nn.Module):
    def __init__(self, obs_size):
        super(ModelCritic, self).__init__()

        self.value = nn.Sequential(
            nn.Linear(obs_size, hidden_layer1_critic),
            nn.ReLU(),
            nn.Linear(hidden_layer1_critic, hidden_layer2_critic),
            nn.ReLU(),
            nn.Linear(hidden_layer2_critic, 1),
        )

    def forward(self, x):
        return self.value(x)


class ModelSACTwinQ(nn.Module):
    def __init__(self, obs_size, act_size):
        super(ModelSACTwinQ, self).__init__()

        self.q1 = nn.Sequential(
            nn.Linear(obs_size + act_size, hidden_layer1_twinQ),
            nn.ReLU(),
            nn.Linear(hidden_layer1_twinQ, hidden_layer2_twinQ),
            nn.ReLU(),
            nn.Linear(hidden_layer2_twinQ, 1),
        )

        self.q2 = nn.Sequential(
            nn.Linear(obs_size + act_size, hidden_layer1_twinQ),
            nn.ReLU(),
            nn.Linear(hidden_layer1_twinQ, hidden_layer2_twinQ),
            nn.ReLU(),
            nn.Linear(hidden_layer2_twinQ, 1),
        )

    def forward(self, obs, act):
        x = torch.cat([obs, act], dim=1)
        return torch.squeeze(self.q1(x),-1), torch.squeeze(self.q2(x),-1)

class AgentSAC(ptan.agent.BaseAgent):
    def __init__(self, net, device="cpu"):
        self.net = net
        self.device = device
    def __call__(self, states, agent_states):
        with torch.no_grad():
            states = ptan.agent.float32_preprocessor(states).to(self.device)
            #:Convert list of states into the form suitable for model
            mu,_=self.net(states,deterministic=False,with_logprob=False) # mean action already sampled from distribution in actor
            actions = mu.data.cpu().numpy()

        return actions, agent_states