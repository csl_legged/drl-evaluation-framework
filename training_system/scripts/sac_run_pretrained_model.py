#!/usr/bin/env python3
# -*- coding: utf-8 -*-

""" Run saved validation models  

Test generalization of the saved model: 

i) Test generalization of the model in varying inclinations 0,10,8,15 degrees
ii) Test generalization in longer inclination of 10 degrees with twice the distance of the trained environment

"""

import gym
import os, os.path
import numpy as np 
import sac_model
import torch 
import laelaps_env_ellipse
import rospy

ENV_ID="LaelapsEnvEllipse-v0"
print("Environment ID",ENV_ID)


saving=True

if __name__ == "__main__":


    model_path=input("Specifiy the pretrained model path from the runs folder: ")
    algorithm=input("Specifiy the algorithm name to be tested and test condition/env: ")

    rospy.init_node('test_model',disable_signals=True, log_level=rospy.INFO) 

    env=gym.make(ENV_ID)
    print("Seed number:",env.seed_num)

    if saving:   
        folder=input("Specifiy the saving folder name: ")
        save_folder= folder+"/PretrainedModel_"+algorithm
        os.makedirs(save_folder) # Main folder
        env.savingpath(save_folder,saving_option=saving)
    else:
        save_folder=os.path.dirname(os.path.realpath(__file__))+"/runs"

    net = sac_model.GaussianActor(env.observation_space.shape[0], env.action_space.shape[0]) # sac
    net.load_state_dict(torch.load(model_path))

    input("Change in Gazebo physics concentraints the ERP value to 0.5 then press ENTER!")
    print("Testing SAC Pre-trained Model")
    test_rewards=0.0
    episode_rewards=[]
    test_number=20
    for i in range(test_number):
        obs = env.reset()
        total_reward = 0.0
        total_steps = 0
        while True:
            obs_v = torch.FloatTensor([obs])
            mu,_= net(obs_v,deterministic=True,with_logprob=False)
            action = mu.squeeze(dim=0).data.numpy()
            obs, reward, done, _ = env.step(action)
            total_reward += reward
            total_steps += 1
            if done:
                test_rewards+=total_reward
                episode_rewards.append(total_reward)
                print("----------Terminated----------TEST: ", i)
                print("In %d steps we got %.3f reward" % (total_steps, total_reward))
                break
    print("Mean total reward in all tests: ",test_rewards/test_number)
    if saving: 
        np.save(save_folder+"/TestRewards_"+algorithm,episode_rewards)


